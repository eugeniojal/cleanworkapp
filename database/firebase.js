import firebase from 'firebase'

import "firebase/firestore"
// Your web app's Firebase configuration
var firebaseConfig = {
    apiKey: "AIzaSyD_J4_FeyvB7DCAP5-z960kYGtR9ZP6DhU",
    authDomain: "clean-work.firebaseapp.com",
    projectId: "clean-work",
    storageBucket: "clean-work.appspot.com",
    messagingSenderId: "580495546858",
    appId: "1:580495546858:web:937af6c2bb6bec2cdeba93"
};
// Initialize Firebase
firebase.initializeApp(firebaseConfig);

const db = firebase.firestore();

export default {
    firebase,
    db,
};