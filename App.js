import { StatusBar } from 'expo-status-bar';
import React from 'react';
import { StyleSheet, Text, View } from 'react-native';
import {NavigationContainer} from '@react-navigation/native'
import {createStackNavigator} from '@react-navigation/stack'
import Logged from './screens/logged'
import Login from './screens/login'
import { LogBox } from 'react-native';

LogBox.ignoreAllLogs();//Ignore all log notifications
  const Stack = createStackNavigator()

import UserList from './screens/UserList'
import CreateUserScreen from './screens/CreateUserScreen'
import Userdetail from './screens/UserDetail'


function MyStack() {
  return(
    <Stack.Navigator>
      {/* <Stack.Screen name="UserList1" component={UserList} /> */}
      <Stack.Screen name="UserList" component={Login} />
      <Stack.Screen name="CreateUserScreen" component={CreateUserScreen} />
      <Stack.Screen name="Userdetail" component={Userdetail} />
      <Stack.Screen name="Logged" component={Logged} />
       
    </Stack.Navigator>
  );
}


export default function App() {
  return (
<NavigationContainer>
      <MyStack/>
</NavigationContainer>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
  },
});
