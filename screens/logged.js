import React, { useState, useEffect } from "react";
import { StyleSheet, Text, View, Image } from "react-native";
import * as Location from "expo-location";
import {
    Provider as PaperProvider,
    Appbar,
    Button,
    Card,
    Title,
    Avatar,
} from "react-native-paper";


export default function Logged() {
    let text;

    const [location, setLocation] = useState(null);
    const [errorMsg, setErrorMsg] = useState(null);
    const [address, findAddress] = useState(null);
    const [load, changeLoad] = useState(false);


    useEffect(() => {
        (async () => {
            let { status } = await Location.requestForegroundPermissionsAsync();
            if (status !== "granted") {
                setErrorMsg("Permission to access location was denied");
                return;
            }
        })();
    }, []);

    const styles = StyleSheet.create({
        container: {
            height: 500,
            backgroundColor: "#f7e9d9",
            alignItems: "center",
        },
        containerImage: {
            backgroundColor: "#f7e9d9",
            width: 400,
            height: 250,
            justifyContent: "center",
            alignItems: "center",
        },
        tinyLogo: {
            width: 200,
            height: 120,
        }, buttom: {
            backgroundColor: address == null ? "#2ea3f2" : "orange",
            width: 200,
            height: 50,
            justifyContent: "center",
            marginTop: 30
        }
    });

    const obtenerUbicacion = async () => {
        changeLoad(true);
        let location = await Location.getCurrentPositionAsync({});
        setLocation(location);
        let address = await Location.reverseGeocodeAsync({
            latitude: location.coords.latitude,
            longitude: location.coords.longitude,
        });
        changeLoad(false);
        findAddress(address);
    };

    const marcarSalida = () => {
        findAddress(null);
    };

    if (errorMsg) {
        text = errorMsg;
    } else if (address) {
        console.log(address);
        text = JSON.stringify(address);
    }

    return (
        <PaperProvider>
            {/* <Appbar.Header style={{ backgroundColor: "#2ea3f2" }}>
        <Appbar.BackAction />
        <Appbar.Content title="Asistencia" subtitle="Clean Work Services 1" />
        <Appbar.Action
          icon="dots-vertical"
        
        />
      </Appbar.Header> */}
            <View style={styles.containerImage}>
                <Image
                    style={styles.tinyLogo}
                    source={{
                        uri: "https://cleanworkservices1.com/wp-content/uploads/2021/05/icono.png",
                    }}
                />
            </View>

            <View style={styles.container}>
                <Card style={{ backgroundColor: "#fb931a", height: 300, width: 300 }}>
                    <Card.Content
                        style={{ alignItems: "center", justifyContent: "center" }}
                    >
                        <Avatar.Image
                            size={100}
                            source={{
                                uri: "https://st4.depositphotos.com/27867620/30520/v/600/depositphotos_305207868-stock-illustration-user-web-icon-simple-design.jpg",
                            }}
                        />
                        <Title >Bienvenido Juvenal</Title>
                        {address == null ? (
                            <Button
                                loading={load}
                                style={{
                                    backgroundColor: "#2ea3f2",
                                    width: 200,
                                    height: 50,
                                    justifyContent: "center",
                                    marginTop: 30
                                }}
                                icon="map"
                                mode="contained"
                                onPress={obtenerUbicacion}
                            >
                                Marcar entrada
                            </Button>
                        ) : (
                            <Button
                                //loading = {load}
                                style={styles.buttom}
                                icon="car"
                                mode="contained"
                                onPress={marcarSalida}
                            >
                                Marcar salida
                            </Button>
                        )}
                    </Card.Content>
                </Card>

                <Text style={styles.paragraph}>{text}</Text>
            </View>
        </PaperProvider>
    );
}