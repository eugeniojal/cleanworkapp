// In App.js in a new project
import React, { useState }  from "react";
import { View } from "react-native";
import { Button, TextInput } from "react-native-paper";
import firebase from '../database/firebase'
import { LogBox } from 'react-native';

export default function Login(navigatio) {
    const [user, setUser] = useState("");
    const [pass, setPass] = useState("");
    const [usuario, setUsuario] = useState([]);

    

    const Entrar = async() => {
        
       const query =  firebase.db.collection("users").where("name", "==", user)
        const datos = await query.get()
            .then((querySnapshot) => {
                querySnapshot.forEach((doc) => {
                    const  { name, correo, tlf } = doc.data()
                    usuario.push({
                        id: doc.id,
                        name,
                        correo,
                        tlf
                    })
                    setUsuario(usuario)
                });
            });
        
        console.log(usuario)
       
        if (usuario[1].tlf == pass) {
            // () => navigation.navigation.navigate('UserList');
            navigatio.navigation.navigate('Logged');
            // alert(usuario[1].tlf)
        }else{
            alert('USUARIO O CONTRASEÑA INVALIDA')
        }
    }

    return (
        <View style={{ flex: 1, alignItems: "center", justifyContent: "center" }}>
            <TextInput
                style={{ width: 300 }}
                mode="outlined"
                label="Email o Usuario"
                value={user}
                onChangeText={(text) => setUser(text)}
            />

            <TextInput
            
                style={{ width: 300 }}
                mode="outlined"
                label="Password"
                value={pass}
                onChangeText={(text) => setPass(text)}
            />

            <Button
                style={{ marginTop: 20 }}
                mode="contained"
                onPress={Entrar}
            >
                Entrar
      </Button>
        </View>
    );
}