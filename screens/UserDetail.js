import React,{useEffect} from 'react'
import { View, Text } from 'react-native'
import firebase from '../database/firebase'

const Userdetail = (props) => {
    
    const getUserById = async (id) => {
          const dbRef =   firebase.db.collection('users').doc(id)
          const doc = await dbRef.get();
          const user = doc.data();
          console.log(user)
    }

    useEffect(() => {
        getUserById(props.route.params.UserId);
    })

    return (
        <View>
            <Text>DETALLE</Text>
        </View>
    );
}

export default Userdetail