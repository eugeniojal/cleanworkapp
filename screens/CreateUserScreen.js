import React from 'react'
import { useState } from 'react'
import { View, Button, TextInput,ScrollView, StyleSheet} from 'react-native'
import firebase from '../database/firebase'
const CreateUserScreen = (props) => {

    const[state,setState] = useState({
        nombre:'',
        correo:'',
        tlf:''
    })

    const cambiarVariable = (name,value) =>{
        setState({ ...state,[name]:value})
    } 

    const NuevoUsuario = async () =>{
        if (state.nombre === ''){
            alert('por favor introduzca un nombre')
        }else{
            await firebase.db.collection('users').add({
                name: state.nombre,
                correo: state.correo,
                tlf: state.tlf
            })
            props.navigation.navigate('UserList');
        }
    }
    return (
        <ScrollView style={styles.container}>
            <View style={styles.inputGroup}>
                <TextInput placeholder="Nombre de usuario"
                onChangeText={(value) => cambiarVariable('nombre',value)} />
            </View>
            <View style={styles.inputGroup}>
                <TextInput placeholder="correo de usuario" 
                onChangeText={(value) => cambiarVariable('correo', value)}/>
            </View>
            <View style={styles.inputGroup}>
                <TextInput placeholder="Numero de telefono"
                onChangeText={(value) => cambiarVariable('tlf', value)} />
            </View>
            <View >
                <Button title="Guardar" onPress={() => NuevoUsuario() } />
            </View>
        </ScrollView>

    )
}

const styles = StyleSheet.create({
    container:{
        flex:1,
        padding:35
    },
    inputGroup:{
        flex:1,
        padding:0,
        marginBottom:15,
        borderBottomWidth:1,
        borderBottomColor:'#947FCD',
        
    }
})
export default CreateUserScreen