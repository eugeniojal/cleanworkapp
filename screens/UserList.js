import  React, {useState,useEffect} from 'react'
import {View,Text,ScrollView, Button} from 'react-native'
import firebase from '../database/firebase'
import {ListItem,Avatar} from 'react-native-elements'




const UserList = (props) => {

    const [users,setUsers] = useState([])

    useEffect(() => {
        firebase.db.collection('users').onSnapshot(querySnapshot => {

            const users  = [];
            querySnapshot.docs.forEach( doc => {
                const {name,correo,tlf} = doc.data()
                users.push({
                    id:doc.id,
                    name,
                    correo,
                    tlf
                })
            })
            setUsers(users)
        })
    }, [])

    return(
        <ScrollView>
            <Button title="CREAR USUARIO"
            onPress={() => props.navigation.navigate('CreateUserScreen') } />

            {
                users.map(user => {
                    return(
                        <ListItem  
                            key={user.id}
                         bottomDivider
                         onPress={() => {
                             props.navigation.navigate('Userdetail',{
                                 UserId:user.name
                             }
                             )
                         }}
                        >
                        <ListItem.Chevron/>
                            <Avatar 
                            source={{ uri:'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcR9digPIQu9wAxrcog6Utn9QO_JqA3x3Dd2pQ&usqp=CAU'}}
                            rounded
                            />
                        <ListItem.Content>
                            <ListItem.Title>{user.name}</ListItem.Title>
                            <ListItem.Subtitle>{user.correo}</ListItem.Subtitle>
                        </ListItem.Content>


                        </ListItem>


                    )
                })
            }
        </ScrollView>
    );
}

export default UserList